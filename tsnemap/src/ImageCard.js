import React, {
  useEffect,
  useState,
  useRef,
} from 'react';

import {
  Sprite,
  SpriteMaterial,
  Vector3,
  CanvasTexture,
  TextureLoader,
  sRGBEncoding,
} from 'three';

import useInterval from 'react-useinterval';

import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import CloseIcon from '@material-ui/icons/Close';
import {
  Card,
  CardContent,
  Typography,
  IconButton,
  makeStyles
} from '@material-ui/core';

import {
  objectToScreenPosition,
  vec3dToScreenPosition,
} from './common/objectToScreenPosition';

const lqRes = require('./globals').lqRes;

const useStyles = makeStyles({
  root: {
    margin: "16px 0",
  }
});

const areEqual = (a, b) => {
  if(a.highlighted !== b.highlighted || a.selected !== b.selected || a.atlasMap !== b.atlasMap) {
    return false;
  }
  return true;
};

const ImageCard = React.memo(props => {
  const classes = useStyles();
  const elementRef = useRef();

  const [loaded, setLoaded] = useState(false);
  const [object, setObject] = useState();
  const lqMap = useRef();
  const hqMap = useRef();
  useEffect(() => {
    if(
      props.threeDeeApp === undefined ||
      props.cardContainer === undefined ||
      props.atlasMap === undefined ||
      props.atlasItem === undefined ||
      props.atlasRes === undefined) {
      return;
    }

    const canvas = document.createElement('canvas');
    canvas.width = lqRes;
    canvas.height = lqRes;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(props.atlasMap,
      props.atlasItem.x*props.atlasRes,
      props.atlasItem.y*props.atlasRes,
      lqRes, lqRes, 0, 0, lqRes, lqRes);
    const map = new CanvasTexture(canvas);
    lqMap.current = map;

    const spriteMaterial = new SpriteMaterial({ map: map, color: 0xffffff });
    const o = new Sprite(spriteMaterial);
    o.center.set(0.5, 0);
    o.scale.setScalar(0.01);
    props.cardContainer.add(o);
    o.position.set(props.item.position.x, props.item.position.y, props.item.position.z);
    setObject(o);
    setLoaded(true);
  }, [
    props.atlas,
    props.atlasItem,
    props.atlasMap,
    props.atlasRes,
    props.cardContainer,
    props.item,
    props.threeDeeApp,
  ]);

  useInterval(() => {
    if(object === undefined) {
      return;
    }
    if(props.threeDeeApp.camera.position.distanceTo(object.position) < 0.1) {
      if(hqMap.current === undefined) {
        const spriteMap = new TextureLoader().load("images/" + props.item.filename, () => {
          spriteMap.encoding = sRGBEncoding;
          hqMap.current = spriteMap;
          object.material.map = hqMap.current;
          props.threeDeeApp.forceNextRender = true;
          props.threeDeeApp.render();
        });
      } else {
        object.material.map = hqMap.current;
      }
    } else {
      object.material.map = lqMap.current;
    }
  }, 1000);

  const prevLoaded = useRef(false);
  useEffect(() => {
    if(loaded && prevLoaded.current === false) {
      props.loaded(object);
    }
    prevLoaded.current = loaded;
  }, [
    loaded,
    object,
    props,
  ]);

  useEffect(() => {
    if(object === undefined) {
      return;
    }

    if(props.highlighted && !props.selected) {
      object.material.color.setRGB(0.75, 0.75, 0.25);
    } else {
      object.material.color.setRGB(1, 1, 1);
    }
    props.threeDeeApp.forceNextRender = true;
  }, [
    object,
    props.highlighted,
    props.selected,
    props.threeDeeApp,
  ])

  const [selected, setSelected] = useState(false);

  useEffect(() => {
    const postRender = () => {
      if(elementRef.current && object && props.threeDeeApp) {
        var bottomRightVector = new Vector3(0.005,0, 0);
        bottomRightVector.applyQuaternion(props.threeDeeApp.camera.quaternion);
        const screenBottomLeft = vec3dToScreenPosition(props.threeDeeApp, object.position.clone().add(bottomRightVector));
        const screenBottomRight = vec3dToScreenPosition(props.threeDeeApp, object.position.clone().addScaledVector(bottomRightVector, -1));

        const width = screenBottomRight.distanceTo(screenBottomLeft);
        const fac = 5;
        elementRef.current.style.width =  fac*100+ "px";
        elementRef.current.style.height = fac*20 + "px";

        const pos = objectToScreenPosition(props.threeDeeApp, object);
        elementRef.current.style.left = 0;
        elementRef.current.style.top = 0;
  
        const scale = 1/fac * 0.01*width;
        elementRef.current.style.transform = "translate(" + pos.x + "px, " + pos.y + "px) scale(" + (scale !== undefined ? scale : 1) + ") translate(-50%, 0%)";
        elementRef.current.style.zIndex = Math.floor(scale*100000);
        
        let targetPosition = new Vector3();
        targetPosition = targetPosition.setFromMatrixPosition(object.matrixWorld);
        let cameraPos = new Vector3().setFromMatrixPosition(props.threeDeeApp.camera.matrixWorld);
        let p = targetPosition.sub(cameraPos);
        let target = new Vector3();
        props.threeDeeApp.camera.getWorldDirection(target);
        const behindCamera = ((p.angleTo(target)) > (Math.PI / 2));
        elementRef.current.style.display = behindCamera ? "none" : "flex";
      }
    };
    if(props.selected) {
      setSelected(true);
      props.threeDeeApp.eventBus.on('post-render', postRender);
    } else {
      setSelected(false);
      props.threeDeeApp.eventBus.detach('post-render', postRender);
    }
  }, [
    object,
    props.selected,
    props.threeDeeApp,
  ]);

  const controlPanel = (
    <Card
      ref={elementRef}
      style={{
        position: "absolute",
        transformOrigin: "top left",
        backgroundColor: "white",
        display: !loaded ? "flex" : "none",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        overflow: "none",
        border: "none",
        pointerEvents: "all",
      }}
    >
      <div
        style={{
          width: "100%",
        }}
      >
        <CardContent
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            padding: 0,
            paddingTop: "16px",
            paddingLeft: "16px",
            margin: "0",
          }}
          classes={{
            root: classes.root,
          }}
        >
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            style={{
              margin: 0,
            }}
          >
          {"Title: " + props.item.title}
          <IconButton href={"./images/" + props.item.filename} target="_blank">
            <OpenInNewIcon fontSize="small" />
          </IconButton>
          </Typography>
          <IconButton onClick={props.onClose}>
            <CloseIcon fontSize="small" />
          </IconButton>
        </CardContent>
      </div>
      <CardContent>
        Coordinates (X/Y/Z): ({Math.round(props.item.position.x*100)/100}, {Math.round(props.item.position.y*100)/100}, {Math.round(props.item.position.z*100)/100})
      </CardContent>
    </Card>
  );

  return (
    (!loaded || selected) ? controlPanel : null
  );
}, areEqual);
export default ImageCard;
