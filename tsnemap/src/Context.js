import React from 'react'

export const Context = React.createContext(true);

let initialState = {
  cardContainer: undefined,
  highlightedObject: undefined,
  selectedObject: undefined,
  threeDeeApp: undefined,
  tsneData: undefined,
  atlas: undefined,
  allLoaded: false,
};

let reducer = (state, action) => {
  switch (action.type) {
    case "set-highlightedObject":
      if(state.highlightedObject === action.payload) {
        return state;
      }
      return { ...state, highlightedObject: action.payload };
    case "set-selectedObject":
      return { ...state, selectedObject: state.highlightedObject };
    case "set-threedeeapp":
      return { ...state, threeDeeApp: action.payload };
    case "set-tsneData":
      return { ...state, tsneData: action.payload };
    case "set-atlas":
      return { ...state, atlas: action.payload };
    case "set-cardContainer":
      return { ...state, cardContainer: action.payload };
    case "set-allLoaded":
      return { ...state, allLoaded: action.payload };
    default:
      return { ...state };
  }
};

export function ContextProvider(props) {
  let [state, dispatch] = React.useReducer(reducer, initialState);
  let value = { state, dispatch };

  return (
    <Context.Provider value={value}>{props.children}</Context.Provider>
  );
}

export const ContextConsumer = Context.Consumer;
