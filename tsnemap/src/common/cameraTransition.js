const TWEEN = require('@tweenjs/tween.js').default;

export default function cameraTransition(app, newCamPos, newTargetPos, duration) {
  new TWEEN.Tween(app.camera.position)
  .to({
    x: newCamPos.x,
    y: newCamPos.y,
    z: newCamPos.z,
  }, duration)
  .easing(TWEEN.Easing.Quadratic.InOut)
  .start();
  new TWEEN.Tween(app.controls.target)
  .to({
    x: newTargetPos.x,
    y: newTargetPos.y,
    z: newTargetPos.z,
  }, duration)
  .easing(TWEEN.Easing.Quadratic.InOut)
  .start();
}