
import {
  Vector2,
  Vector3,
} from 'three';

export function objectToScreenPosition(app, obj)
{
  var vector = new Vector3();

  var widthHalf = 0.5*app.renderer.domElement.parentElement.getBoundingClientRect().width;
  var heightHalf = 0.5*app.renderer.domElement.parentElement.getBoundingClientRect().height;

  obj.updateMatrixWorld();
  vector.setFromMatrixPosition(obj.matrixWorld);
  vector.project(app.camera);

  vector.x = ( vector.x * widthHalf ) + widthHalf;
  vector.y = - ( vector.y * heightHalf ) + heightHalf;

  return new Vector2(
    vector.x,
    vector.y
  );
}

export function vec3dToScreenPosition(app, vec3d)
{
  var vector = new Vector3();

  var widthHalf = 0.5*app.renderer.domElement.parentElement.getBoundingClientRect().width;
  var heightHalf = 0.5*app.renderer.domElement.parentElement.getBoundingClientRect().height;

  vector = vec3d.clone();
  vector.project(app.camera);

  vector.x = ( vector.x * widthHalf ) + widthHalf;
  vector.y = - ( vector.y * heightHalf ) + heightHalf;

  return new Vector2(
    vector.x,
    vector.y
  );
}
