import React, {
  useContext,
  useEffect,
  useRef,
} from 'react';

import './ThreeDeeViewport.css';
import { Context } from './Context'

import { Application } from 'threedeejs';
import {
  AmbientLight,
  Color,
  MOUSE,
  Vector3,
  // AxesHelper,
} from 'three';
// import { Sky } from 'three/examples/jsm/objects/Sky';
import cameraTransition from './common/cameraTransition';

export default function ThreeDeeViewport_ (props) {
  const {state, dispatch} = useContext(Context);
  const viewport = useRef(undefined);

  // Setup 3D viewport and load scene
  useEffect(() => {
    const App = new Application(
      viewport.current,
      {
        hdriFilename: "./assets3d/envmap.jpg",
        ambientLight: new AmbientLight(new Color(1, 1, 1), 0.5),
        fpsLimit: 60,
        enableRendering: true,
        autoRemoveLoadingScreen: false,
      },
      () => {
        const cameraInitialPos = new Vector3(0.5, 2, 0);  //(0.5, 10, 0)
        window.App = App;
        App.controls.target.set(0.6, 0, 0.3);  // 0.5, 0, 0.5
        App.camera.position.copy(cameraInitialPos);
        App.camera.near = 0.01; // 0.01
        App.camera.updateProjectionMatrix();
        App.controls.mouseButtons = {
          LEFT: MOUSE.PAN,
          MIDDLE: MOUSE.DOLLY,
          RIGHT: MOUSE.ROTATE,
        }
        App.controls.minPolarAngle = Math.PI/4;
        App.controls.maxPolarAngle = Math.PI/2;
        App.controls.dampingFactor = .7; // Albert 0.075
        App.controls.zoomSpeed = 1.; // Albert 0.25 the mouse scroll but, pinch speed
        App.controls.rotateSpeed = 0.3; // super slow rotation speed (right-clicking)
        App.controls.panSpeed = 0.9; // super slow pan speed (left-clicking)
        App.controls.enabled = false; // This should be false in the beginning, so that the user cannot move the camera, while the intro animation is running
        App.controls.minDistance = 0.1; // 0.1
        // var sky = new Sky();
        // sky.material.uniforms.sunPosition.value.set(0, 1, 0);
        // sky.material.uniforms.rayleigh.value = 2;
				// sky.scale.setScalar(10);
				// App.scene.add( sky );
        dispatch({ type: "set-threedeeapp", payload: App });
        App.renderer.domElement.style.outline = "none";

        // // START
        // // DEBUGGING CODE
        // // Axes helper to visualize world origin
        // App.scene.add(new AxesHelper(5));
        // // Axes helper to visualize camera target
        // const camTarget = new AxesHelper(0.1);
        // camTarget.position.copy(App.controls.target);
        // App.eventBus.on('pre-render', () => {
        //   camTarget.position.copy(App.controls.target);
        // });
        // App.scene.add(camTarget);
        // // END
        // // DEBUGGING CODE
      }
    );
  }, [dispatch]);

  useEffect(() => {
    if(state.threeDeeApp === undefined || !state.allLoaded) {
      return;
    }
    const cameraFinalPos = new Vector3(1, 0.12, 0.03);  //  0.6, 0.25, 0.0, 1, 0.25, 0
    cameraTransition(state.threeDeeApp, cameraFinalPos, state.threeDeeApp.controls.target, 1100); //Albert 3000 intro effect
    setTimeout(() => {
      state.threeDeeApp.controls.enabled = true;
      state.threeDeeApp.controls.maxDistance = 1; // limit how far the user can zoom out, Albert - remarked
    }, 1100); //Albert 3000 intro effect
  }, [
    state.allLoaded,
    state.threeDeeApp,
  ]);

  return (
    <div className="ThreeDeeViewport" ref={viewport}>
    </div>
  );
}
