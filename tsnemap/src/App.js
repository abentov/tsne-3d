import React from 'react';

import { ContextProvider } from './Context';
import { Main } from './Main';

function App() {
  return (
    <ContextProvider>
      <Main />
    </ContextProvider>
  );
}

export default App;
