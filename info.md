09-03-2020 12:38
### `yarn start`

## Try to copy-and-paste this code to the console:
setInterval(() => {console.log("Camera position: ", App.camera.position, "Camera target: ", App.controls.target);}, 5000);

03-03-2020 11:34

## docs, reference
https://threejs.org/docs/#examples/en/controls/OrbitControls

## BG, Background
Example for radial gradient as background:  
https://gitlab.com/abentov/tsne-3d/-/commit/2529df506e722b790d43ceb34ae39ccf106e9c67  
**tsnemap/src/ThreeDeeViewport.css - line ~6**  

Example for solid background color:  
https://gitlab.com/abentov/tsne-3d/-/commit/811a0907f2853a7c860d02b42021664679b76979  
**tsnemap/src/ThreeDeeViewport.css - line ~6**  

## Initial camera stuff
The initial position of the camera is set here:  
https://gitlab.com/abentov/tsne-3d/-/blob/fixes/tsnemap/src/ThreeDeeViewport.js#L36

The initial position of the camera's target (the point it looks at) is set here:  
https://gitlab.com/abentov/tsne-3d/-/blob/fixes/tsnemap/src/ThreeDeeViewport.js#L38

The position of the camera, after the intro animation is completed is set here:  
https://gitlab.com/abentov/tsne-3d/-/blob/fixes/tsnemap/src/ThreeDeeViewport.js#L69


## Zoom in/out
b) when a box is zoomed, and someone decides to click on another box in the background - can we instead of just un-zooming the zoomed one, also zoom-in the clicked box in the background? Currently only un-zoom happens
9156c38a36f390bff835fce88f172155bbbf5742  
**tsnemap/src/TSNEViewer.js line 61**  

## Drag/ Mouse speed control
c) 60. By default, ThreeJS's OrbitControls just have one set of "speed" values for both mouse and touch input. If you need to adjust the speeds separately, it would be possible to modify ThreeJS's OrbitControls.

I don't think I need separate controls for mobile and desktop - just the location in code of where to control those

c) You can just add another line somewhere here:  
https://gitlab.com/abentov/tsne-3d/-/blob/master/tsnemap/src/ThreeDeeViewport.js#L50  
`App.controls.rotateSpeed = 0.01; // super slow rotation speed (right-clicking)`
`App.controls.panSpeed = 0.01; // super slow pan speed (left-clicking)`
See: https://threejs.org/docs/#examples/en/controls/OrbitControls

---

make the **initial zoom**, after the intro be **larger**:
f42218c73699e439dd8be1ed65e59a37924a3c5b
**tsnemap/src/ThreeDeeViewport.js - line ~33**  
**tsnemap/src/ThreeDeeViewport.js - line ~48**  
`-        App.controls.maxDistance = 1;`  
`-                               sky.scale.setScalar( 5 );`  
`+                               sky.scale.setScalar(10);`  
**tsnemap/src/ThreeDeeViewport.js - line ~68**  
`cameraTransition(state.threeDeeApp, cameraFinalPos, state.threeDeeApp.controls.target, 1700); //Albert 3000 intro effect`  
`+      state.threeDeeApp.controls.maxDistance = 1; // limit how far the user can zoom out`

use solid background color:
811a0907f2853a7c860d02b42021664679b76979
**tsnemap/src/ThreeDeeViewport.css - line ~6**  
`-        const cameraInitialPos = new Vector3(0.5, 2, 0);`  
`  +        const cameraInitialPos = new Vector3(0.5, 10, 0);`  
**tsnemap/src/ThreeDeeViewport.js - line ~48**
`-        App.controls.enabled = true; // Albert false`
`+        App.controls.enabled = false; // This should be false in the beginning, so that the user cannot move the camera, while the intro animation is running`

use radial gradient as scene background:
2529df506e722b790d43ceb34ae39ccf106e9c67  
**tsnemap/src/ThreeDeeViewport.css - line ~6** - set radial bg/background
**tsnemap/src/ThreeDeeViewport.js - line ~17, line ~53** - rm sky

### first commit by Albert (after all changes)
0e5623dd6c29dd4c50ca569430002ad935ea1bd4
