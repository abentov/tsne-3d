import React, {
  useContext, useEffect, useRef, useState,
} from 'react';

import { Object3D, Vector3, ImageBitmapLoader } from 'three';
import Hammer from 'hammerjs';

import { Context } from './Context'
import ImageCard from './ImageCard';
import cameraTransition from './common/cameraTransition';

const zoomInTime = 1000; // Albert's value: 370
const zoomOutTime = 1000; // Albert's value: ~700

export default function TSNEViewer (props) {
  const {state, dispatch} = useContext(Context);

  useEffect(() => {
    fetch('tsnedata.json')
      .then(data => data.json())
      .then(json => {
        dispatch({ type: "set-tsneData", payload: json });
      });
    fetch('atlas/atlas.json')
      .then(data => data.json())
      .then(json => {
        dispatch({ type: "set-atlas", payload: json });
      });
  }, [dispatch]);

  useEffect(() => {
    if(state.threeDeeApp === undefined) {
      return;
    }

    const cardContainer = new Object3D();
    state.threeDeeApp.scene.add(cardContainer);
    dispatch({ type: "set-cardContainer", payload: cardContainer });
  }, [
    dispatch,
    state.threeDeeApp,
  ]);

  useEffect(() => {
    if(state.threeDeeApp === undefined) {
      return;
    }
    const onTap = e => {
      const tap = (e, highlightObject = undefined) => {
        if(e.pointerType === "mouse") {
          setTimeout(() => {
            dispatch({ type: "set-highlightedObject", payload: highlightObject });
            dispatch({ type: "set-selectedObject" });
          }, 0);
        } else {
          setTimeout(() => {
            dispatch({ type: "set-highlightedObject", payload: highlightObject });
            dispatch({ type: "set-selectedObject" });
          }, 250);
        }
      };

      if(state.selectedObject === undefined) {
        tap(e, state.highlightedObject);
        return;
      }

      var viewDirection = new Vector3();
      state.threeDeeApp.camera.getWorldDirection(viewDirection);
      const newCamPos = state.selectedObject.position.clone().addScaledVector(viewDirection, -0.2);
      const newTargetPos = state.selectedObject.position.clone();
      newCamPos.y += 0.02;
      cameraTransition(state.threeDeeApp, newCamPos, newTargetPos, zoomOutTime); //Albert - not clear? on tap?

      const nextHighlightedObject = state.highlightedObject;
      setTimeout(() => {
        tap(e, nextHighlightedObject);
        state.threeDeeApp.controls.enableDamping = nextHighlightedObject === undefined;
      }, zoomOutTime); //Albert 1000 - not clear? on tap?
    }
    var hammertime = new Hammer(state.threeDeeApp.renderer.domElement.parentElement);
    hammertime.on('tap', onTap);
    return () => {
      hammertime.off('tap', onTap);
    }
  }, [
    dispatch,
    state.highlightedObject,
    state.threeDeeApp,
    state.selectedObject,
  ]);

  useEffect(() => {
    if(state.threeDeeApp === undefined) {
      return;
    }
    state.threeDeeApp.controls.enabled = state.selectedObject === undefined;
    if(state.threeDeeApp.controls.enabled) {
      state.threeDeeApp.controls.enableDamping = true;
    }
    if(state.selectedObject !== undefined) {
      var viewDirection = new Vector3();
      state.threeDeeApp.camera.getWorldDirection(viewDirection);
      const newCamPos = state.selectedObject.position.clone().addScaledVector(viewDirection, -0.02);
      const newTargetPos = newCamPos.clone().add(state.threeDeeApp.controls.target.clone().sub(state.threeDeeApp.camera.position));
      newCamPos.y += 0.005;
      newTargetPos.y += 0.005;
      cameraTransition(state.threeDeeApp, newCamPos, newTargetPos, zoomInTime); //Albert 1000 or zoomin is here?
      setTimeout(() => {
        state.threeDeeApp.controls.enableDamping = false;
      }, zoomInTime); //Albert 1000 or zoomin is here?
    }
  }, [
    state.selectedObject,
    state.threeDeeApp,
  ]);

  const tsneData = (state.tsneData !== undefined && state.atlas !== undefined) ? state.tsneData : [];

  const numLoaded = useRef(0);
  const [atlasMap, setAtlasMap] = useState();
  useEffect(() => {
    if(state.threeDeeApp === undefined || state.atlas === undefined) {
      return;
    }
    new ImageBitmapLoader().load("atlas/atlas.jpeg", (imageBitmap) => {
      setAtlasMap(imageBitmap);
      state.threeDeeApp.removeLoadingScreen();
    });
  }, [
    state.atlas,
    state.threeDeeApp,
  ]);

  const objects = useRef({});

  return (
    <div
      style={{
        position: "absolute",
        width: "100%",
        height: "100%",
        pointerEvents: "none",
        opacity: state.allLoaded ? "1" : "0",
      }}
    >
      {
        tsneData.map((e, i) => (
          <ImageCard
            key={i}
            item={e}
            atlasRes={state.atlas.res}
            atlasItem={state.atlas.files[e.filename]}
            atlasMap={atlasMap}
            loaded={(object) => {
              numLoaded.current++;
              objects.current[e.filename] = object;
              if(numLoaded.current === state.tsneData.length) {
                dispatch({ type: "set-allLoaded", payload: true });
              }
            }}
            highlighted={objects.current[e.filename] === state.highlightedObject}
            selected={objects.current[e.filename] === state.selectedObject}
            threeDeeApp={state.threeDeeApp}
            cardContainer={state.cardContainer}
            onClose={e => {
              var viewDirection = new Vector3();
              state.threeDeeApp.camera.getWorldDirection(viewDirection);
              const newCamPos = state.selectedObject.position.clone().addScaledVector(viewDirection, -0.2);
              const newTargetPos = state.selectedObject.position.clone();
              newCamPos.y += 0.02;
              cameraTransition(state.threeDeeApp, newCamPos, newTargetPos, zoomOutTime); //Albert 1000 on box close
              setTimeout(() => {
                dispatch({ type: "set-highlightedObject", payload: undefined });
                dispatch({ type: "set-selectedObject" });
                state.threeDeeApp.controls.enableDamping = false;
              }, zoomOutTime); //Albert 1000 on box close
            }}
          />
        ))
      }
    </div>
  );
}
