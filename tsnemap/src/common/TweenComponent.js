import {
  useEffect,
} from 'react';

const TWEEN = require('@tweenjs/tween.js').default;

export default function TweenComponent() {
  useEffect(() => {
    function animate(time) {
      window.requestAnimationFrame(animate);
      TWEEN.update(time);
    }
    animate();
  }, []);
  return null;
}
