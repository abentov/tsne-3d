import React, {
  useContext,
} from 'react';
import './Main.css';

import { Context } from './Context'
import ThreeDeeViewport from './ThreeDeeViewport';
import TSNEViewer from './TSNEViewer';
import TweenComponent from './common/TweenComponent';
import Raycaster from './common/Raycaster';

export function Main() {
  const {state, dispatch} = useContext(Context);

  return (
    <div
      className="Main"
    >
      <TweenComponent />
      <ThreeDeeViewport />
      <TSNEViewer />
      <Raycaster
        threeDeeApp={state.threeDeeApp}
        interval={1000 / 15}
        objects={state.cardContainer?.children}
        setIntersectObjects={intersect => {
          dispatch({ type: "set-highlightedObject", payload: intersect.length ? intersect[0].object : undefined });
        }}
      />
    </div>
  );
}
