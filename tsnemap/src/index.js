import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

if ( ! ( 'createImageBitmap' in window ) ) {
	window.createImageBitmap = async function ( blob ) {
		return new Promise( ( resolve, reject ) => {
			let img = document.createElement( 'img' );
			img.addEventListener( 'load', function () {
				resolve( this );
			} );
			img.src = URL.createObjectURL( blob );
		} );
	};
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
