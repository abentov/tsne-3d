import {
  useEffect,
  useRef,
  useState,
} from 'react';

import { Raycaster as THREE_Raycaster, Vector2 } from 'three';
import Hammer from 'hammerjs';

import useInterval from 'react-useinterval';

export default function Raycaster(props) {
  const [raycaster, ] = useState(new THREE_Raycaster());
  const mouseX = useRef();
  const mouseY = useRef();

  const renderer = props.threeDeeApp !== undefined ? props.threeDeeApp.renderer : undefined;
  const camera = props.threeDeeApp !== undefined ? props.threeDeeApp.camera : undefined;

  useEffect(() => {
    if(renderer === undefined) {
      return;
    }
    function onMouseMove(x, y) {
      const clientRect = renderer.domElement.getBoundingClientRect();
      mouseX.current = ((x - clientRect.left) / clientRect.width) * 2 - 1;
      mouseY.current = -((y - clientRect.top) / clientRect.height) * 2 + 1;
    }
    renderer.domElement.parentElement.addEventListener('mousemove', event => {
      onMouseMove(event.clientX, event.clientY);
    }, false);
    var hammertime = new Hammer(renderer.domElement.parentElement);
    hammertime.on('tap', event => {
      onMouseMove(event.center.x, event.center.y);
    });
  }, [
    renderer,
  ]);

  useInterval(() => {
    if(mouseX.current === undefined || mouseY.current === undefined || camera === undefined) {
      props.setIntersectObjects([]);
      return;
    }
    raycaster.setFromCamera(new Vector2(mouseX.current, mouseY.current), camera);
    props.setIntersectObjects(raycaster.intersectObjects(props.objects, props.recursive));
  }, props.interval);
  
  return null;
}
