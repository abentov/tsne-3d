const fs = require('fs');
const path = require('path');
const lqRes = require(path.join(__dirname, 'src', 'globals')).lqRes;
const {
  createCanvas,
  loadImage,
} = require('canvas');

async function main() {
  const imageFolder = path.join(__dirname, 'public', 'images');
  const files = await fs.readdirSync(imageFolder);
  const numFiles = files.length;
  let numColumns = Math.floor(Math.sqrt(numFiles));
  if(Math.sqrt(numFiles)-numColumns > 0) {
    numColumns += 1;
  }
  let numRows = Math.ceil(numFiles/numColumns);
  const canvas = createCanvas(numColumns*lqRes, numRows*lqRes);
  const ctx = canvas.getContext("2d");

  const atlasJson = {
    res: lqRes,
    numColumns,
    numRows,
    files: {},
  };
  files.forEach(async (f, i) => {
    const myimg = await loadImage(path.join(imageFolder, files[i]));
    const x = i%numColumns;
    const y = Math.floor(i/numColumns);
    ctx.drawImage(myimg, x*lqRes, y*lqRes, lqRes, lqRes);
    atlasJson.files[f] = {
      x, y
    }
  })

  const atlasFolder = path.join(__dirname, 'public', 'atlas');
  const out = fs.createWriteStream(path.join(atlasFolder, 'atlas.jpeg'));
  const stream = canvas.createJPEGStream();
  stream.pipe(out)
  out.on('finish', () =>  {
    console.log('The JPEG file was created.')
    var json = JSON.stringify(atlasJson);
    fs.writeFile(path.join(atlasFolder, 'atlas.json'), json, 'utf8', () => {
      console.log('The JSON file was created.')
    });
  })
}

main();
